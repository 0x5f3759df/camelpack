#!/bin/bash
docker run -u root -v "$PWD":/home/gradle/project -w /home/gradle/project  gradle:5.2.1-jre8-alpine  gradle --no-daemon clean assemble
docker run -v "$PWD:/usr/src/camelpack" -w /usr/src/camelpack node:8 bash -c 'npm install && npm run build'
