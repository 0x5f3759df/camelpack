FROM openjdk:8u171-alpine3.7
RUN apk --no-cache add curl

RUN mkdir build

COPY build/js build/js
COPY build/libs/camelpack-*.jar camelpack.jar
CMD java -Djava.naming.factory.initial=org.apache.camel.util.jndi.CamelInitialContextFactory ${JAVA_OPTS} -jar camelpack.jar
