# CamelPack

A manager for Apache Camel routes written using Groovy DSLs

## What is it?

Apache Camel is a super powerful way to connect different components together in 
messaging systems. It is however, awkward to configure routes: you either have to
mess around with XML to define your routes or write full fledged programs if 
you want something more flexible. And then once you do that, they can't be easily
dynamically redefined on the fly.

CamelPack provides:

 - A system that lets you define Camel routes using snippets of Groovy code
 - Each set of routes lives in a separate file, and its own Camel Context
 - Any given route can be hot-modified and will dynamically update without restarting
   anything
 - A web interface that lets you view the routes and enable / disable them 


This is all very prototype and immature, more a proof of concept at this stage.

Example: 

In routes/foo.groovy:

```groovy
from('activemq:queueA') to('activemq:queueB')
```

In routes/bar.groovy:

```groovy
from('file:///staging/in') multicast(['file:///processed/','activemq:incomingNotifications'])
```

**NOTE**: the current build expects ActiveMQ to be running on a host called `activemq`. You can 
alter it in the code or define this host in your `/etc/hosts` to point where you want.


## Documentation

See the [User Guide](docs/userguide.adoc)

## Installation / Building

```
git clone https://gitlab.com/ssadedin/camelpack.git
cd camelpack
npm install
npm run build
 ./gradlew run --console plain
```

Then open in your browser:

http://localhost:7775/camel

You should get an interface like this:

![Camel Screenshot](docs/screenshot.png)



