/**
 *
 * Camel Pack 
 *
 * This service monitors the route folder and allows routes to be added / modified / enabled / disabled dynamically
 *
 */
package camelpack

import static org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent;

import org.apache.camel.CamelContext
import org.apache.camel.Exchange
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.file.GenericFile
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.impl.SimpleRegistry

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.text.SimpleTemplateEngine
import groovy.text.Template
import groovy.text.TemplateEngine
//import ch.grengine.*
import groovy.transform.CompileStatic
import groovy.util.logging.Log4j
import io.micronaut.scheduling.annotation.Scheduled

import org.jivesoftware.smackx.jiveproperties.*;

import org.apache.log4j.BasicConfigurator
import org.apache.log4j.Level
import org.apache.log4j.Logger


import javax.inject.Singleton

class RouteDefinition {
    
    RouteDefinition(File routeFile) {
        List<String> nonImportLines = []
        List<String> importLines = []
        
        routeFile.eachLine { String line ->
            if(line.startsWith('import'))
                importLines << line
            else
                nonImportLines << line
        }
        
        this.imports = importLines.join('\n')
        this.routes = nonImportLines.join('\n')
    }
    
    SimpleRegistry registry
    String imports 
    String routes
    
}

@Singleton
@Log4j
class ContextManager {
    
    @Delegate
    JSONHelpers jsonHelpers = new JSONHelpers()
    
    File routesDir = new File("routes")
    
    File libsDir = new File("routes/lib")
    
    Map<String,Long> routeTimestamps = [:]
    
    List<Route> routes = []
    
    Map globals = Collections.synchronizedMap([:])
    
    static {
        BasicConfigurator.configure()
        Logger.getRootLogger().setLevel(Level.INFO)
    }
    
    ContextManager() {
        log.info "Created context manager in directory: " + new File(System.properties['user.dir'])
    }
    
    void setup() {
        getRouteFiles().each { File routeFile ->
            updateRoute(routeFile)
        }
        pauseMissingRoutes()
    }

    private void pauseMissingRoutes() {
        for(Route route in routes) {
            if(!route.file.exists() && route.state != "paused") {
                log.info "File for route $route.name no longer exists: pausing route"
                pause(route.name)
                return
            }
        }
    }
    
    void updateRoute(File routeFile) {
        Route route
        try {
            
            route = getRoute(routeFile)
            
            long routeTimestampMs = route.lastUpdated
            if(routeTimestampMs < routeFile.lastModified()) {
                log.info "Refreshing route $routeFile (modified)"
                setup(route)
            }
        }
        catch(Exception e) {
            log.error("Failed to set up routes from $route.name: $e")
            if(route != null)
                route.state = 'error'
        }
        
    }
    
    List<File> getRouteFiles() {
       routesDir.listFiles().grep { it.name.endsWith('.groovy') } 
    }
    
    List<Route> getRoutes() {
        return routes
    }
    
    Route getRoute(File routeFile) {
        String routeName = routeFile.name.replaceAll('.groovy$','')
        Route route = getRoute(routeName)
        if(route == null) {
            log.info "New route $routeName"
            route = new Route(
                name: routeName,
                lastUpdated: 0L,
                state: 'New',
                file: routeFile
            )
            this.routes << route
        }
        return route
    }
    
    Route getRoute(String name) {
        routes.find { it.name == name }
    }
    

   
   void stop() {
        routes*.context*.stop()
    }
    
    Templates templates = new Templates()
    
    synchronized void setup(Route route) {
        
        CamelContext context = route.context
        if(context)
            context.stop()
            
        if(route.state == 'paused') {
            log.info "Skip setup of $route because it is paused"
            return
        }
        log.info "Setup $route"
            
        context = route.context = new DefaultCamelContext(route.registry)
        String activemqHost = System.getProperty('activemq.host', 'activemq')
        String activemqPort = System.getProperty('activemq.port','61616')
        context.addComponent("activemq", activeMQComponent("tcp://$activemqHost:$activemqPort"));
        context.addRoutes(createRouteBuilder(route))
        context.start()
        route.lastUpdated = route.file.lastModified()
        route.state = 'active'
        log.info "Done updating $route.file"
    }
    
    RouteBuilder createRouteBuilder(Route route) {
        
        RouteDefinition routeDef = new RouteDefinition(route.file)

        List<File> jars = libsDir.listFiles().grep { File libFile -> libFile.name.endsWith('.jar') }
        if(!jars.isEmpty()) {
            log.info "Found jars to add to classpath: " + jars.join(',')
        }
        
        File classesDir = new File(libsDir,'classes')
        
        Logger routeLog = Logger.getLogger("->$route.name<-")
        return new RouteBuilder() {
            void configure() {
              log.info "Evaluate $route.name ($route.file)"
              def binding = new Binding(
                  globals:this.globals, 
                  registry: route.registry, 
                  log: routeLog, 
                  templates: templates,
                  parseJSON: this.&parseJSON,
                  toJSON: this.&toJSON
              )

              GroovyShell shell = new GroovyShell(binding)
              jars.each { 
                  shell.classLoader.addClasspath(it.absolutePath)
              }
              
              if(classesDir.exists()) {
                  shell.classLoader.addClasspath(classesDir.absolutePath)
              }
              
              def router = shell.evaluate(
                  """
                  $routeDef.imports
                  routes = {
                      $routeDef.routes
                  }
                  """.stripIndent()
                  // 'routes = {' + route.file.text + '}'
              )
              log.info "Finished evaluation of $route.file"
              router.delegate = this
              router()
            }
        }        
    }
    
   
    void pause(String routeName) {
        Route route = getRoute(routeName)
        if(route.state == 'active') {
            log.info 'Pause route: ' + routeName
            route.state = 'paused'
            route.lastUpdated = 0
            route.context.stop()
            route.context = null
        }
    }
    
    void unpause(String routeName) {
        Route route = getRoute(routeName)
        if(route.state == 'paused') {
            route.state = 'active'
            route.lastUpdated = 0
            setup(route)
        }
    }
    
    @Scheduled(fixedRate = "10s")
    void refreshRoutes() {
        setup()
    }
}