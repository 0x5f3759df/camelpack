package camelpack.components

import java.util.regex.Pattern

import org.apache.camel.Consumer
import org.apache.camel.Endpoint
import org.apache.camel.EndpointConfiguration
import org.apache.camel.Exchange
import org.apache.camel.ExchangePattern
import org.apache.camel.Processor
import org.apache.camel.Producer
import org.apache.camel.component.file.GenericFile
import org.apache.camel.impl.DefaultComponent
import org.apache.camel.impl.DefaultEndpoint
import org.apache.camel.impl.DefaultExchange
import org.apache.camel.impl.ScheduledPollConsumer
import org.apache.camel.spi.UriEndpoint; 
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.IssuesApi
import org.gitlab4j.api.NotesApi
import org.gitlab4j.api.models.FileUpload
import org.gitlab4j.api.models.Issue
import org.gitlab4j.api.models.IssueFilter
import org.gitlab4j.api.models.Project
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import camelpack.BoundTemplate
import camelpack.ContextManager
import camelpack.Templates
import groovy.json.JsonSlurper
import groovy.text.SimpleTemplateEngine
import groovy.text.TemplateEngine

class GitlabProducer implements Producer {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabProducer")

    GitlabEndpoint endpoint
    
    TemplateEngine templateEngine = new SimpleTemplateEngine()
    
    public GitlabProducer(GitlabEndpoint endpoint) {
        super();
        this.endpoint = endpoint;
        
        if(endpoint.labels==null && endpoint.title == null && endpoint.titlePattern == null && endpoint.search == null) {
            throw new IllegalArgumentException("Cannot create producer for endpoint $endpoint One of labels, title, titlePattern or search must be specified")
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        logger.info "Procesing exchange $exchange"
        
        List<Issue> issues = endpoint.queryIssues()
        
        logger.info "Found ${issues.size()} issues matching $endpoint"
        
        if(endpoint.maxIssues > 0 && issues.size() > endpoint.maxIssues) {
            logger.info "Taking first ${endpoint.maxIssues} of ${issues.size()} due to maxIssues=$endpoint.maxIssues"
            issues = issues.take(endpoint.maxIssues)
        }
        
        def body = exchange.in.body
        def content
        if(endpoint.template) {
            logger.info "Replacing body with template $endpoint.template"
            
            def bodyAsMap = (exchange.in.body instanceof Map ? exchange.in.body : [body: exchange.in.body, exchange: exchange]) 
            
            body = ((GitlabComponent)endpoint.component).templates.methodMissing(endpoint.template, [bodyAsMap])
        }
        
        if(body instanceof GenericFile) {
            logger.info "Body is file $body.absoluteFilePath : converting to file contents"
            content = body.file.text
        }
        else
        if(body instanceof BoundTemplate) {
            
            // If there are files in the template, upload them and replace references with
            // Gitlab upload paths
            body = uploadAndReplaceAttachmentReferences(body)

            content = body.toString()
        }
        else {
            content = body.toString()
        }
        
        if(issues.isEmpty()) {
            if(endpoint.title == null) 
                throw new IllegalArgumentException("Title must be specified for creation of new issues in Gitlab Producer")

            logger.info "No issue found matching search criteria: will create issue"

            endpoint.gitlab.issuesApi.createIssue(endpoint.gitlabProject, endpoint.title, content)
        }
        else {
            for(Issue issue in issues) {
                endpoint.gitlab.notesApi.createIssueNote(endpoint.gitlabProject.id, issue.iid, content)
            }
        }
        
    }
    
    BoundTemplate uploadAndReplaceAttachmentReferences(BoundTemplate body) {
        
        GitlabEndpoint ep = endpoint
        
        Map gitlabVariables = body.variables.collectEntries { entry ->
            File file = null
            if(entry.value instanceof GenericFile) {
                file = ((GenericFile)entry.value).file
            }
            else
            if(entry.value instanceof File) {
                file = entry.value
            }
            
            if(file) {
                // Upload file
                File actualFile = file.absoluteFile
                logger.info "Uploading $actualFile to Gitlab project $ep.gitlabProject.id"
                FileUpload result = ep.gitlab.projectApi.uploadFile(ep.gitlabProject.id, actualFile)
                return [ entry.key, "[$file.name]($result.url)"]
            }
            else {
                return [ entry.key, entry.value ]
            }
        }
        
        return new BoundTemplate(template: body.template, variables:gitlabVariables)
    }

    @Override
    public void start() throws Exception {
        // noop
    }

    @Override
    public void stop() throws Exception {
        //noop
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public Endpoint getEndpoint() {
        return endpoint
    }

    @Override
    public Exchange createExchange() {
        return new DefaultExchange(this.endpoint)
    }

    @Override
    public Exchange createExchange(ExchangePattern pattern) {
        Exchange e = new DefaultExchange(this.endpoint, pattern)
        return e;
    }

    @Override
    public Exchange createExchange(Exchange exchange) {
        return new DefaultExchange(exchange)
    }
}

class GitlabConsumer extends ScheduledPollConsumer implements Runnable {

    GitlabEndpoint endpoint
    
    private static Logger logger = LoggerFactory.getLogger("GitlabConsumer")
    
    public GitlabConsumer(Endpoint endpoint, Processor processor) {
        super(endpoint, processor);
        this.endpoint = endpoint
        
        if(this.endpoint.interval > 0) {
            this.delay = this.endpoint.interval * 1000
        }
        else
        if(super.getDelay() < 5000)
            this.delay = 120000
    }

    @Override
    protected int poll() throws Exception {
       Date now = new Date();

       List<Issue> issues = endpoint.queryIssues()
       
       int processed = 0
       for(Issue issue in issues) {

           Exchange exchange = endpoint.createExchange();
           
           exchange.getIn().setBody(issue)
           getProcessor().process(exchange);
           ++processed
           
           if(endpoint.add || endpoint.remove) {
               String labelAdds = endpoint.add?.tokenize(',')?.collect {
                   '/label ~"'+it+'"'
               }?.join('\n')?:""
               
               String labelRemoves = endpoint.remove?.tokenize(',')?.collect {
                   '/unlabel ~"'+it+'"'
               }?.join('\n')?:""
              
               log.info("Create issue note on issue $issue.id")
               endpoint.gitlab.notesApi.createIssueNote(
                   endpoint.gitlabProject.id, 
                   issue.iid, 
                   (endpoint.note ?: "Processed automatically on $now\n") + "$labelAdds\n$labelRemoves\n"
               )
           }
           ++processed
       }
       return processed
    }

}

/**
 * An endpoint that polls a gitlab project and forwards issues that match a specified
 * title pattern and having / excluding specific labels. To prevent issues being 
 * returned repeatedly, labels can be specified for removal upon processing.
 * 
 * @author Simon Sadedin
 */
@UriEndpoint(scheme="gitlab", syntax="gitlab:name", title="Gitlab", consumerClass=GitlabConsumer)
class GitlabEndpoint extends DefaultEndpoint {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabEndpoint")

    /**
     * Comma separated labels to search for. By default, issues will only be returned if the
     * label is present. However a label prefixed by - will cause issues to be excluded if the 
     * label is found.
     */
    String labels

    String token
    
    String search
    
    String titlePattern
    
    /**
     * Used only for producer: title of issue to create
     */
    String title
    
    /**
     * Project to poll issues from. By default, this is the name of the host in the endpoint, ie:
     * 
     * gitlab:&lt;project&gt;?params ....
     */
    String project
    
    /**
     * Labels to remove when message is processed
     */
    String remove

    /**
     * Labels to add when message is processed
     */
    String add
    
    /**
     * Note to add when issue is processed
     */
    String note
    
    /**
     * Template to apply (Producer only)
     */
    String template
    
    /**
     * Maximum number of issues to process when operating on multiple issues
     */
    int maxIssues = 1
    
    long interval = -1
    
    GitLabApi gitlab 
    
    Project gitlabProject
    
    Pattern titlePatternRegex
    
    GitlabEndpoint(String uri, GitlabComponent component) {
        super(uri, component);
    }

    @Override
    public Consumer createConsumer(Processor processor) throws Exception {
        
        logger.info "Create consumer for " + super.endpointUri + "(key = " + super.endpointKey + ") with path " + super.getEndpointConfiguration().getParameter(EndpointConfiguration.URI_PATH)
        
       
        if(template != null) 
            throw new IllegalArgumentException("Template argument is not supported for Gitlab Consumer")
        
        return new GitlabConsumer(this, processor)
    }
    
    void initGitlabConnection() {

        if(gitlab)
            return // already initialised 

        String gitlabServer = this.token?:System.getenv("GITLAB_SERVER")?:System.getProperty('camel.gitlabServer')
        
        if(!gitlabServer) 
            throw new IllegalStateException("Unable to create Gitlab Component because neither GITLAB_SERVER environment variable or camel.gitlabServer property are set")
            
        String gitlabToken = System.getenv("GITLAB_TOKEN")?:System.getProperty('camel.gitlabToken')
        
        if(!gitlabToken) 
            throw new IllegalStateException("Unable to create Gitlab Component because neither GITLAB_TOKEN environment variable or camel.gitlabToken property are set")
    
        gitlab = new GitLabApi(gitlabServer, gitlabToken)
    
        if(titlePattern) {
            titlePatternRegex = ~titlePattern
        }
        
        String project = this.project ?: new URI(this.endpointUri).host ?: new URI(this.endpointUri).authority 
        
         logger.info "Connecting end point $endpointUri to Gitlab project $project"
        
        try {
            if(project.isInteger()) {
                this.gitlabProject = gitlab.projectApi.getProject(project.toInteger())
            }
            else {
                this.gitlabProject = gitlab.projectApi.getProjects(project)[0]
            }
            
            logger.info "Connected to project $gitlabProject.id ($gitlabProject.name)"
        }
        catch(Exception e) {
            logger.error("Unable to resolve project based on specifier: $project ($e)", e)
        }
    }

    @Override
    public Producer createProducer() throws Exception {

        return new GitlabProducer(this)
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
    
    /**
     * Query for issues matching the labels, title and search parameters of the endpoint
     * 
     * @return List of issues found
     */
    List<Issue> queryIssues() {
       
        this.initGitlabConnection()
         
        Map labels = this.labels
            ?.tokenize(',')
            ?.groupBy { it.startsWith('-') ? 'exclude' : 'require' }
            ?:[:]

        List<Issue> allIssues = this.gitlab.issuesApi.getIssues(this.gitlabProject, new IssueFilter(
                labels : labels.require,
                search: this.search,
                ))

        List<Issue> issues
        if(labels.exclude) {
            issues = allIssues.grep { Issue issue ->
                !labels.exclude.any { it in issue.labels }
            }
        }
        else {
            issues = allIssues
        }
        
        if(this.title) {
            issues = issues.grep { Issue issue -> issue.title == this.title }
        }
        else
        if(this.titlePattern) {
            issues = issues.grep { Issue issue -> issue.title ==~ this.titlePatternRegex }
        }

        logger.info "Found ${issues.size()} issues matching $search with labels $labels.require, excluding $labels.exclude"
        return issues
    }
    
}

class GitlabComponent extends DefaultComponent {
    
    private static Logger logger = LoggerFactory.getLogger("GitlabComponent")

    Templates templates = new Templates()
    
    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception { 
        
        Endpoint endpoint = new GitlabEndpoint(uri, this);
        setProperties(endpoint, parameters);
        return endpoint; 
    }
}


