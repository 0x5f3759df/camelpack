package camelpack

class Messagable extends Reader {
    
    private StringReader reader
  
    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        if(reader == null) {
            reader = new StringReader(this.toString())
        }
        return reader.read(cbuf, off, len)
    }

    @Override
    public void close() throws IOException {
        // noop
    }
    
    String toString() {
        new JSONHelpers().toJSON(this)
    }
}
