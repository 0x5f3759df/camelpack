package camelpack

import static org.apache.activemq.camel.component.ActiveMQComponent.*

import java.util.concurrent.BlockingQueue
import java.util.regex.Pattern
import org.apache.camel.CamelContext
import org.apache.camel.Component
import org.apache.camel.Endpoint
import org.apache.camel.Exchange
import org.apache.camel.Expression
import org.apache.camel.ProducerTemplate
import org.apache.camel.RoutesBuilder
import org.apache.camel.builder.AdviceWithRouteBuilder
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.component.stub.StubComponent
import org.apache.camel.component.stub.StubEndpoint
import org.apache.camel.test.junit4.CamelTestSupport

import groovy.json.JsonOutput

class TestRecipient {

}

/**
 * Provides enhanced support for testing of Camelpack routes on top of normal Camel 
 * test support.
 * 
 * This class extends the usual Camel test support class with additional features to make it
 * easy to test Camelpack routes. These include:
 * 
 * <li>Automatic loading of the routes, by setting the {@link#testContext} field in your
 *     test class constructor.
 * <li>Convenience methods to send JSON messages to routes for testing purposes
 * <li>Automatic construction of mocks, by setting the {@link#mockPatterns} field in your
 *      test class constructor
 * <li>Convenience methods to retrieve output messages from mocked routes as JSON or raw exchanges,
 *     with built in wait / timeout functionality
 *
 * For general information on testing with Camel, see: https://camel.apache.org/components/latest/mock-component.html
 * 
 * @author Simon Sadedin
 */
class CamelpackTestSupport extends CamelTestSupport {
    
   @Delegate
   JSONHelpers jsonHelpers = new JSONHelpers()
    
   String activemqHost = System.getProperty('activemq.host', 'activemq')
    
   String activemqPort = System.getProperty('activemq.port', '61616')
   
   String testContext
   
   Map<String,String> mocks
   
   List<String> mockPatterns = []
   
   ContextManager contextManager = new ContextManager()
   
   long mockTimeoutMs = 5000
   
   CamelpackTestSupport(String context) {
       this.testContext = context
   }
   
   class ExchangeMockHelper {
       Object getProperty(String mockName) {
           def mockURI = 'mock:'+mockName
           
           assert CamelpackTestSupport.this.mockPatterns : "You have not configured any mock patterns in your test class. Please set the mockPatterns attribute to match end points you wish to mock"
           
           assert getMockEndpoint(mockURI) : """ 
                   No mock of end point $mockName could be found matching mock patterns ${CamelpackTestSupport.this.mockPatterns}.
              
                   Please check that the mockPatterns attribute of your test class is set correctly and matches the expected routes
                   found in route file ${CamelpackTestSupport.this.testContext}.
               """.trim().stripIndent()

           long startMs = System.currentTimeMillis()
           while(System.currentTimeMillis() - startMs < CamelpackTestSupport.this.mockTimeoutMs) {
                def e = getMockEndpoint(mockURI).receivedExchanges
                if(e) {
                    return e
                }

                Thread.sleep(20)
           }
           throw new AssertionError(""" 
                   No messages could be found to mocked endpoint:

                      $mockName (URI=$mockURI) 

                   after ${CamelpackTestSupport.this.mockTimeoutMs}ms. Please check that the mockPatterns attribute:

                       ${CamelpackTestSupport.this.mockPatterns}

                   is set correctly and matches the expected routes found in route file: ${CamelpackTestSupport.this.testContext}. 

                   Alternatively, check that the timeout of ${CamelpackTestSupport.this.mockTimeoutMs}ms is sufficient to 
                   allow the messages to arrive in time, and adjust the mockTimeoutMs attribute 
                   accordingly.
               """.stripIndent())
       }
   }
    
   
   ExchangeMockHelper exchanges = new ExchangeMockHelper()
   
   class JSONMessageMockHelper {
       Object getProperty(String mockName) {
           def mockURI = 'mock:'+mockName
           
           assert getMockEndpoint(mockURI) : """ 
                   No mock of end point $mockName could be found. Please check your mockup statements to ensure this 
                   mock was configured correctly in route file ${CamelpackTestSupport.this.testContext}.
               """.trim().stripIndent()

           long startMs = System.currentTimeMillis()
           while(System.currentTimeMillis() - startMs < CamelpackTestSupport.this.mockTimeoutMs) {
                def e = getMockEndpoint(mockURI).receivedExchanges
                if(e) {
                    def result = e*.in*.body.collect { 
                        def value = String.valueOf(it)
                        return CamelpackTestSupport.this.jsonHelpers.parseJSON(value) 
                    }
                    return result
                }

                Thread.sleep(20)
           }
           throw new AssertionError(""" 
                   No messages could be found to mocked endpoint:

                      $mockName (URI=$mockURI) 

                   after ${CamelpackTestSupport.this.mockTimeoutMs}ms. Please check your mockup statements to ensure this 
                   mock was configured correctly in route file ${CamelpackTestSupport.this.testContext}.

                   Alternatively, check that the timeout of ${CamelpackTestSupport.this.mockTimeoutMs} is sufficient to 
                   allow the messages to arrive in time, and adjust the mockTimeoutMs attribute 
                   accordingly.
               """.stripIndent())
       }
   }
   
    
   JSONMessageMockHelper messages =  new JSONMessageMockHelper()
   
   class TestSender {
    
        Object value

        void to(String uri) {
            println "Sending to $uri"
            template.sendBody(uri, toJSON(value))
            println "Sent"
        }
    }
    
    TestSender send(Object value) {
        if(value instanceof File) {
            value = parseJSON(value)
        }
        TestSender t = new TestSender() 
        t.value = value
        return t
    }
    
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();
        camelContext.addComponent("activemq", activeMQComponent("tcp://$activemqHost:$activemqPort"));
        return camelContext;
    }

    @Override
    protected RoutesBuilder createRouteBuilder() throws Exception {
        if(!this.testContext)
            return

        Route route = new Route(
                name: testContext,
                lastUpdated: 0L,
                state: 'New',
                file: new File("routes/${testContext}.groovy")
        )
        return contextManager.createRouteBuilder(route)
    }
    
    MockEndpoint getMock(String name) {
        if(!this.mocks) 
            throw new IllegalArgumentException("""
                A reference to a mock named $name was made but no mocks have been declared.
                Mocks should be declared in the "mocks" variable by assigning a map with 
                a key naming each mock and associated value representing the mock pattern.
            """.stripIndent())

        if(!this.mocks.containsKey(name)) {
            throw new IllegalArgumentException("""
                A reference to a mock named $name was made but this does not match any 
                mock declared in the "mocks" variable. The mocks that were declared are:

                ${mocks*.key}

                Please check the reference to $name and make sure a mock is registered with this name.
            """.stripIndent())
        }
        
        Pattern uriPrefixPattern = ~('mock://' + this.mocks[name])
        
//        MockEndpoint me = getMockEndpoint('mock:' + name, false)
//        if(me)
//            return me
//         
        this.context.getEndpoints()
            .grep { it instanceof MockEndpoint }
            .find { MockEndpoint mee -> 
                println("Checking end point $mee.endpointUri"); 
                mee.endpointUri.matches(uriPrefixPattern) 
            }
    }
    
    void mockup(@DelegatesTo(strategy=Closure.DELEGATE_FIRST, value=AdviceWithRouteBuilder) Closure c) {
        context.routeDefinitions[0].adviceWith(context, new AdviceWithRouteBuilder() {
            @Override
            public void configure() {
                c.delegate = this
                c()
            }
        });
    }
    
    @Override
    String isMockEndpointsAndSkip() {
        
        this.mockPatterns = this.mocks*.value
        
        mockPatterns.collect { "($it)" }.join("|") 
    }
}
