package camelpack

import groovy.transform.ToString
import org.apache.camel.CamelContext
import org.apache.camel.impl.SimpleRegistry
import com.fasterxml.jackson.annotation.JsonIgnore

@ToString(includeNames=true, excludes=['definition'])
class Route {
    
    String name
    
    File file
    
    String state
    
    String error
    
    long lastUpdated
    
    @JsonIgnore
    SimpleRegistry registry = new SimpleRegistry()
    
    @JsonIgnore
    CamelContext context
    
    String getDefinition() {
        file.text
    }
}
