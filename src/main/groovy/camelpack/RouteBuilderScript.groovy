package camelpack

import org.apache.camel.builder.RouteBuilder

class RouteBuilderScript extends Script {
    
    Map<String,Object> globals
    
    
    Object parseJSON(Object value) {
        
    }
    
    String toJSON(Object value) {
        
    }
    
    org.apache.camel.model.RouteDefinition from(Object... args) {
    }
    
    org.apache.camel.model.OnExceptionDefinition onException(Throwable... args) {
    }
    
    @Override
    public Object run() {
        // Not used
        return null;
    }
}
